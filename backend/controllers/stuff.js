const Pangolin = require('../models/pangolin');
exports.createThing =  (req, res, next) => {
    delete req.body._id;
  
    const pangolin = new Pangolin({
      ...req.body
    });
    pangolin.save()
      .then(() => res.status(201).json({ message: 'Objet enregistré !'}))
      .catch(error => res.status(400).json({ error }));
  };

  exports.getAllStuff = (req, res, next) => {
    Pangolin.find()
      .then(things => res.status(200).json(things))
      .catch(error => res.status(400).json({ error }));
  };

  exports.getOneThing =  (req, res, next) => {
    Pangolin.findOne({ _id: req.params.id })
      .then(thing => res.status(200).json(thing))
      .catch(error => res.status(404).json({ error }));
  };

 

  exports.modifyThing = (req, res, next) => {
    Pangolin.updateOne({ _id: req.params.id }, { ...req.body, _id: req.params.id })
      .then(() => res.status(200).json({ message: 'Objet modifié !'}))
      .catch(error => res.status(400).json({ error }));
  };
  
  exports.deleteThing =  (req, res, next) => {
    Pangolin.deleteOne({ _id: req.params.id })
      .then(() => res.status(200).json({ message: 'Objet supprimé !'}))
      .catch(error => res.status(400).json({ error }));
  };

