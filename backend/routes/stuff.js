const Book = require('../models/pangolin');
const express = require('express');
const stuffCtrl = require('../controllers/stuff');

const router = express.Router();
router.post('/',stuffCtrl.createThing);
const auth = require('../middleware/auth');

router.get('/',auth, stuffCtrl.getAllStuff);
router.post('/',auth, stuffCtrl.createThing);
router.get('/:id',auth, stuffCtrl.getOneThing);
router.put('/:id',auth, stuffCtrl.modifyThing);
router.delete('/:id',auth, stuffCtrl.deleteThing);



module.exports = router;