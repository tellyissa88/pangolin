const mongoose = require('mongoose');

const thingSchema = mongoose.Schema({
  age: { type: Number, required: true },
  famille: { type: String, required: true },
  race: { type: String, required: true },
  nourriture: { type: String, required: true },
  userId: { type: String, required: true},
  relation: { type: String, required: false },
});

module.exports = mongoose.model('Thing', thingSchema);