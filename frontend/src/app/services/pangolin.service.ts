import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Pangolin } from '../models/pangolin.model';

@Injectable({
  providedIn: 'root'
})
export class PangolinService {
  constructor(private http: HttpClient) {}
  pangolin: Pangolin[] = [];

  public pangolin$ = new Subject<Pangolin[]>();
  getStuff() {
    this.http.get('http://localhost:3000/api/stuff').subscribe(
      (pangolin : Pangolin[]) => {
        if (pangolin) {
          this.pangolin = pangolin;
          this.emitStuff();
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
  emitStuff() {
    this.pangolin$.next(this.pangolin);
  }
  getThingById(id: string) {
    return new Promise((resolve, reject) => {
      this.http.get('http://localhost:3000/api/stuff/' + id).subscribe(
        (response) => {
          resolve(response);
        },
        (error) => {
          reject(error);
        }
      );
    });
  }
  createNewThing(pangolin: Pangolin) {
    return new Promise((resolve, reject) => {
      this.http.post('http://localhost:3000/api/stuff', pangolin).subscribe(
        (response) => {
          resolve(response);
        },
        (error) => {
          reject(error);
        }
      );
    });
  }

  modifyThing(id: string, pangolin: Pangolin) {
    return new Promise((resolve, reject) => {
      this.http.put('http://localhost:3000/api/stuff/' + id, pangolin).subscribe(
        (response) => {
          resolve(response);
        },
        (error) => {
          reject(error);
        }
      );
    });
  }

  deleteThing(id: string) {
    return new Promise((resolve, reject) => {
      this.http.delete('http://localhost:3000/api/stuff/' + id).subscribe(
        (response) => {
          resolve(response);
        },
        (error) => {
          reject(error);
        }
      );
    });
  }
}
