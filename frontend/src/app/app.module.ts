import { AuthInterceptor } from './interceptors/auth-interceptor';
import { LoginComponent } from './auth/login/login.component';
import { PangolinService } from './services/pangolin.service';
import { AuthGuardService } from './services/auth-guard.service';
import { AuthService } from './services/auth.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { SignupComponent } from './auth/signup/signup.component';
import { HeaderComponent } from './header/header.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { RouterModule, Routes} from '@angular/router';
import { PangolinListComponent } from './pangolin-list/pangolin-list.component';
import { SinglePangolinComponent } from './pangolin-list/single-pangolin/single-pangolin.component';
import { PangolinFormComponent } from './pangolin-list/pangolin-form/pangolin-form.component';
import { PangolinModifyComponent } from './pangolin-list/pangolin-modify/pangolin-modify.component';

const appRoutes: Routes = [
  { path: 'auth/signup', component: SignupComponent },
  { path: 'auth/login', component: LoginComponent },
  { path: 'pangolins', component: PangolinListComponent },
  { path: 'pangolin/new', component: PangolinFormComponent },
  { path: 'single/:id',  component: SinglePangolinComponent },
  { path: 'edit/:id', component: PangolinModifyComponent },
  { path: '', redirectTo: 'books', pathMatch: 'full' },
  { path: '**', redirectTo: 'books' }
];

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    LoginComponent,
    HeaderComponent,
    PangolinListComponent,
    SinglePangolinComponent,
    PangolinFormComponent,
    PangolinModifyComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    AuthService,
    AuthGuardService,
    PangolinService,
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
