import { AuthService } from './../../services/auth.service';
import { Pangolin } from './../../models/pangolin.model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PangolinService } from 'src/app/services/pangolin.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pangolin-form',
  templateUrl: './pangolin-form.component.html',
  styleUrls: ['./pangolin-form.component.scss']
})
export class PangolinFormComponent implements OnInit, OnDestroy  {

  public thingForm: FormGroup;
  public errorMessage: string;
  public id: string;
  public userId: string;

  constructor(private formBuilder: FormBuilder,
              private pangolinService: PangolinService,
              private router: Router, private auth: AuthService) { }

  ngOnInit() {
    this.thingForm = this.formBuilder.group({
      age: [6, Validators.required],
      famille: ['famille', Validators.required],
      race: ['race', Validators.required],
      nourriture: ['nourriture', Validators.required]
    });
    this.userId =  this.auth.userId;
  }
  onSubmit() {
    console.log(this.userId);
    const thing = new Pangolin();
    thing.age = this.thingForm.get('age').value;
    thing.famille = this.thingForm.get('famille').value;
    thing.race = this.thingForm.get('race').value;
    thing.nourriture = this.thingForm.get('nourriture').value;
    thing.userId = this.userId;
    thing._id = new Date().getTime().toString();
    console.log(thing);
    this.pangolinService.createNewThing(thing).then(
      () => {
        this.thingForm.reset();
        this.router.navigate(['/pangolins']);
      }
    ).catch(
      (error) => {
        this.errorMessage = error.message;
      }
    );
  }

    ngOnDestroy() {
  }


}

