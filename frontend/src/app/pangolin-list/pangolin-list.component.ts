import { AuthService } from './../services/auth.service';
import { Pangolin } from './../models/pangolin.model';
import { Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { PangolinService } from '../services/pangolin.service';

@Component({
  selector: 'app-pangolin-list',
  templateUrl: './pangolin-list.component.html',
  styleUrls: ['./pangolin-list.component.scss']
})
export class PangolinListComponent implements OnInit, OnDestroy {

  public pangolin: Pangolin[] = [];
  private stuffSub: Subscription;
  public pangolins: Pangolin;
  errorMessage: string;
  userId: string;

constructor(
            private pangolinService: PangolinService,
            private router: Router, private auth: AuthService) { }

  ngOnInit() {
    this.stuffSub = this.pangolinService.pangolin$.subscribe(
      (pangolin) => {
        this.pangolin = pangolin;
      }
    );
    this.pangolinService.getStuff();
    this.userId =  this.auth.userId;
  }
  onProductClicked(id: string) {
      this.router.navigate(['/single/' + id]);
  }
  onGonew(){
    this.router.navigate(['pangolin/new']);
}
onadd(id: string) {
  const ide = id;
  const thing = new Pangolin();
  this.pangolinService.getThingById(id).then(
        (pangolin: Pangolin) => {
          this.pangolins = pangolin;
          thing.age = this.pangolins.age;
          thing.race = this.pangolins.race;
          thing.nourriture = this.pangolins.nourriture;
          thing.userId = this.pangolins.userId;
          thing._id = this.pangolins._id;
          thing.relation = this.pangolins.userId;
          thing.relation = this.userId;
          this.pangolinService.modifyThing(ide, thing).then(
            () => {
              this.ngOnInit();
            },
            (error) => {
              this.errorMessage = error.message;
            }
          );
        });
  this.pangolinService.getStuff();
  this.router.navigate(['/pangolins']);
}
ondelete(id: string) {
  const ide = id;
  const thing = new Pangolin();
  this.pangolinService.getThingById(id).then(
        (pangolin: Pangolin) => {
          this.pangolins = pangolin;
          thing.age = this.pangolins.age;
          thing.race = this.pangolins.race;
          thing.nourriture = this.pangolins.nourriture;
          thing.userId = this.pangolins.userId;
          thing._id = this.pangolins._id;
          thing.relation = this.pangolins.userId;
          thing.relation = null;
          this.pangolinService.modifyThing(ide, thing).then(
            () => {
              this.ngOnInit();
            },
            (error) => {
              this.errorMessage = error.message;
            }
          );
        });
  this.pangolinService.getStuff();
  this.router.navigate(['pangolins']);
}
  ngOnDestroy() {
    this.stuffSub.unsubscribe();
  }
}

