import { AuthService } from './../../services/auth.service';
import { PangolinService } from './../../services/pangolin.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Pangolin } from 'src/app/models/pangolin.model';
import { ActivatedRoute, Router, Params } from '@angular/router';

@Component({
  selector: 'app-single-pangolin',
  templateUrl: './single-pangolin.component.html',
  styleUrls: ['./single-pangolin.component.scss']
})
export class SinglePangolinComponent implements OnInit, OnDestroy{

  public pangolin: Pangolin;
  public userId: string;

  constructor(private router: Router, private route: ActivatedRoute,
              private pangolinService: PangolinService, private auth: AuthService ) { }

  ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => {
        this.pangolinService.getThingById(params.id).then(
          (pangolin: Pangolin) => {
            this.pangolin = pangolin;
          }
        );
      }
    );
    this.pangolinService.getStuff();
    this.userId =  this.auth.userId;
  }
  onGoBack() {
      this.router.navigate(['/pangolins']);
  }
  onClickedDelete() {
      this.pangolinService.deleteThing(this.pangolin._id).then(
        () => {
          this.router.navigate(['/pangolins']);
        }
      );
    }
    onClickedEdit() {
      this.router.navigate(['/edit/' + this.pangolin._id]);
  }
  ngOnDestroy() {
  }
}
