import { AuthService } from './../../services/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Pangolin } from './../../models/pangolin.model';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { PangolinService } from 'src/app/services/pangolin.service';

@Component({
  selector: 'app-pangolin-modify',
  templateUrl: './pangolin-modify.component.html',
  styleUrls: ['./pangolin-modify.component.scss']
})
export class PangolinModifyComponent implements OnInit {
  pangolin: Pangolin;
    thingForm: FormGroup;
    errorMessage: string;
    private partSub: Subscription;
    userId: string;
   constructor( private formBuilder: FormBuilder,
                private route: ActivatedRoute,
                private router: Router,
                private booksService: PangolinService,
                private auth: AuthService) { }

    ngOnInit() {
      this.thingForm = this.formBuilder.group({
        age: [null, Validators.required],
        famille: [null, Validators.required],
        race: [0, Validators.required],
        nourriture: [null, Validators.required]
      });
      this.route.params.subscribe(
        (params) => {
          this.booksService.getThingById(params.id).then(
            (pangolin: Pangolin) => {
              this.pangolin = pangolin;
              this.thingForm.get('age').setValue(this.pangolin.age);
              this.thingForm.get('famille').setValue(this.pangolin.famille);
              this.thingForm.get('race').setValue(this.pangolin.race);
              this.thingForm.get('nourriture').setValue(this.pangolin.nourriture);
            }
          );
        }
      );
      this.userId =  this.auth.userId;
    }
    onSubmit() {
      const pangolin = new Pangolin();
      pangolin.age = this.thingForm.get('age').value;
      pangolin.famille = this.thingForm.get('famille').value;
      pangolin.race = this.thingForm.get('race').value;
      pangolin.nourriture = this.thingForm.get('nourriture').value;
      pangolin._id = new Date().getTime().toString();
      pangolin.userId = this.userId;
      this.booksService.modifyThing(this.pangolin._id, pangolin).then(
        () => {
          this.thingForm.reset();
          this.router.navigate(['/pangolins']);
        },
        (error) => {
          this.errorMessage = error.message;
        }
      );
    }
  }
